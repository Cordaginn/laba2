﻿using BaloonsGame.ViewModels;
using Xamarin.Forms;

namespace BaloonsGame.Views
{
    public class GameStateView : ContentView
    {
        Label fillLabelOne;
        Label fillLabelTwo;
        Label score;
        Label minutes;
        Label seconds;
        public GameStateVM gameState;
        StackLayout guiPartOne;
        StackLayout guiPartTwo;
        public GameStateView()
        {
            fillLabelTwo = new Label { Text = ":", TextColor = Color.White };
            fillLabelOne = new Label { Text = "Score:", TextColor = Color.White };
            score = new Label { TextColor = Color.White };
            seconds = new Label { TextColor = Color.White };
            minutes = new Label { TextColor = Color.White };
            gameState = new GameStateVM { Score = 0, Minutes = 0, Seconds = 0 };
            seconds.BindingContext = gameState;
            minutes.BindingContext = gameState;
            score.BindingContext = gameState;
            seconds.SetBinding(Label.TextProperty, "Seconds");
            minutes.SetBinding(Label.TextProperty, "Minutes");
            score.SetBinding(Label.TextProperty, "Score");

            guiPartOne = new StackLayout
            {
                HorizontalOptions = LayoutOptions.StartAndExpand,
                Orientation = StackOrientation.Horizontal,
                Children =
                {
                    fillLabelOne,
                    score
                }
            };
            guiPartTwo = new StackLayout
            {
                HorizontalOptions = LayoutOptions.EndAndExpand,
                Orientation = StackOrientation.Horizontal,
                Children =
                {
                    minutes,
                    fillLabelTwo,
                    seconds
                }
            };
            Content = new StackLayout
            {
                VerticalOptions = LayoutOptions.StartAndExpand,
                Orientation = StackOrientation.Horizontal,
                Children = {
                    guiPartOne,
                    guiPartTwo
                }
            };
        }
    }
}