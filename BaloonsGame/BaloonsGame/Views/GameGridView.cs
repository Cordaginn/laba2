﻿using BaloonsGame.ViewModels;
using System.Collections.Generic;
using Xamarin.Forms;

namespace BaloonsGame.Views
{
    public class GameGridView : ContentView
    {
        public List<BalloonVM> balloonsCollection;
        public Grid gameField;
        public GameGridView()
        {
            gameField = new Grid();
            for (int i = 0; i < GameSettings.GameGridRowSize; i++)
            {
                gameField.RowDefinitions
                    .Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            }
            for (int i = 0; i < GameSettings.GameGridColumnSize; i++)
            {
                gameField.ColumnDefinitions
                    .Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            }
            balloonsCollection = new List<BalloonVM>();
            Content = new StackLayout
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Children = {

                    gameField
                }
            };
        }
    }
}