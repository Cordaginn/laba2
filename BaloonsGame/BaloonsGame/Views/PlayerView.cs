﻿using BaloonsGame.ViewModels;
using System;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace BaloonsGame.Views
{
    public class PlayerView : ContentView
    {
        public PlayerVM player;
        public BoxView playerBox;
        public Grid playerField;
        public SensorVM sensor;
        public SwipeGestureRecognizer gestureRecognizerLeft;
        public SwipeGestureRecognizer gestureRecognizerRight;
        public PlayerView()
        {
            playerField = new Grid
            {
                RowDefinitions =
                {
                    new RowDefinition {Height = new GridLength(1,GridUnitType.Star)}
                }
            };
            for (int i = 0; i < GameSettings.GameGridColumnSize; i++)
            {
                playerField.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            }

            CreatePlayer();
            SensorsInit();

            Content = new StackLayout
            {
                Children =
                {
                    playerField
                }
            };

        }

        private void CreatePlayer()
        {
            player = new PlayerVM() { PlayerColumnPosition = GameSettings.PlayerInitialPosition, PlayerRowPosition = 0 };
            playerBox = new BoxView() { Color = Color.White };
            playerField.Children.Add(playerBox);
            playerBox.BindingContext = player;
            playerBox.SetBinding(Grid.ColumnProperty, "PlayerColumnPosition");
            playerBox.SetBinding(Grid.RowProperty, "PlayerRowPosition");
        }

        private void SensorsInit()
        {
            gestureRecognizerLeft = new SwipeGestureRecognizer { Direction = SwipeDirection.Left };
            gestureRecognizerRight = new SwipeGestureRecognizer { Direction = SwipeDirection.Right };
            gestureRecognizerRight.Swiped += OnSwiped;
            gestureRecognizerLeft.Swiped += OnSwiped;
            sensor = new SensorVM();
            try
            {
                Accelerometer.Start(GameSettings.SensorSpeed);
                Gyroscope.Start(GameSettings.SensorSpeed);
            }
            finally
            {
                sensor.PropertyChanged += SensorDataChanged;
                Accelerometer.ReadingChanged += AccelerometerChange;
                Gyroscope.ReadingChanged += GyroscopeChange;
            }
        }

        private void OnSwiped(object sender, SwipedEventArgs e)
        {
            if (e.Direction == SwipeDirection.Left && player.PlayerColumnPosition != 0)
            {
                player.PlayerColumnPosition--;
            }
            else
            {
                if (player.PlayerColumnPosition != GameSettings.GameGridColumnSize - 1)
                {
                    player.PlayerColumnPosition++;
                }
            }
        }

        private void SensorDataChanged(object sender, EventArgs e)
        {
            if (sensor.FilteredDataGyro > 2 && sensor.FilteredDataAccel > 1 && player.PlayerColumnPosition != 0)
            {
                player.PlayerColumnPosition--;
            }
            else
            {
                if (sensor.FilteredDataGyro < -2 && sensor.FilteredDataAccel < -1 && player.PlayerColumnPosition != GameSettings.GameGridColumnSize - 1)
                {
                    player.PlayerColumnPosition++;
                }
            }
        }

        private void AccelerometerChange(object sender, AccelerometerChangedEventArgs e)
        {
            sensor.DataAccel = e.Reading.Acceleration;
        }

        private void GyroscopeChange(object sender, GyroscopeChangedEventArgs e)
        {
            sensor.DataGyro = e.Reading.AngularVelocity;
        }
    }
}