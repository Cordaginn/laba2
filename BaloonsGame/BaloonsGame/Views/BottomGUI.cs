﻿using BaloonsGame.Core;
using BaloonsGame.ViewModels;
using System;
using System.Collections.Generic;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace BaloonsGame.Views
{
    public class BottomGUI : ContentView
    {
        public List<BalloonVM> sequence;
        public Grid sequenceView;
        public Button pauseButton;
        public Button resumeButton;
        public Button exitButton;
        public FlexLayout sequenceAndPause;
        public FlexLayout resumeAndExit;
        public BottomGUI()
        {
            sequence = new List<BalloonVM>();
            sequenceView = new Grid();
            pauseButton = new Button { Text = "| |", BackgroundColor = Color.DeepSkyBlue};
            resumeButton = new Button { Text = "Resume", BackgroundColor = Color.YellowGreen};
            exitButton = new Button { Text = "Exit", BackgroundColor = Color.Tomato};
            for (int i = 0; i < GameSettings.SequenceCollectionSize; i++)
            {
                sequenceView.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            }
            sequenceAndPause = new FlexLayout
            {
                JustifyContent = FlexJustify.SpaceBetween,
                Children ={
                    sequenceView,
                    pauseButton
                        }
            };
            resumeAndExit = new FlexLayout
            {
                JustifyContent = FlexJustify.SpaceBetween,
                IsVisible = false,
                Children ={
                            resumeButton,
                            exitButton
                        }
            };
            sequenceView.SetValue(FlexLayout.GrowProperty, 5);
            pauseButton.SetValue(FlexLayout.GrowProperty, 1);
            resumeButton.SetValue(FlexLayout.GrowProperty, 1);
            exitButton.SetValue(FlexLayout.GrowProperty, 1);

            pauseButton.Clicked += Pause;
            resumeButton.Clicked += Resume;
            GameHandler.InitializeSequence(ref sequenceView, ref sequence);

            Content = new StackLayout
            {
                HorizontalOptions = LayoutOptions.EndAndExpand,
                Children = {
                    sequenceAndPause,
                    resumeAndExit
                }
            };
        }

        private void Pause(object sender, EventArgs e)
        {
            sequenceAndPause.IsVisible = false;
            resumeAndExit.IsVisible = true;
            try
            {
                Accelerometer.Stop();
                Gyroscope.Stop();
            }
            finally
            {
                GameHandler.isGamePaused = true;
            }
        }

        private void Resume(object sender, EventArgs e)
        {
            sequenceAndPause.IsVisible = true;
            resumeAndExit.IsVisible = false;
            try
            {
                Accelerometer.Start(GameSettings.SensorSpeed);
                Gyroscope.Start(GameSettings.SensorSpeed);
            }
            finally
            {
                GameHandler.isGamePaused = false;
            }
        }
    }
}