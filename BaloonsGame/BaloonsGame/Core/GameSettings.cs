﻿using Xamarin.Essentials;
using Xamarin.Forms;

namespace BaloonsGame
{
    public static class GameSettings
    {
        public static int FirstTimerInterval = 60 * 1000;
        public static double TimerInterval = 1000;
        public static int GameGridColumnSize = 12;
        public static int GameGridRowSize = 15;
        public static int SequenceCollectionSize = 5;
        public static int MaximumBalloonsInGame = 15;
        public static int PlayerInitialPosition = 6;
        public static SensorSpeed SensorSpeed = SensorSpeed.UI;
        public static Thickness FirstBalloonInSequenceMargin = new Thickness { Left = 0, Bottom = 0, Right = 10, Top = 0 };
    }
}
