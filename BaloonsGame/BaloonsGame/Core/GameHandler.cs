﻿using BaloonsGame.ViewModels;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace BaloonsGame.Core
{
    public static class GameHandler
    {
        private static Random randomizer;
        public static bool isGameValid;
        public static bool isGamePaused;

        public static void InitHandler()
        {
            randomizer = new Random(DateTime.UtcNow.Second + DateTime.UtcNow.Minute);
            isGameValid = true;
            isGamePaused = false;
        }

        public static void HandleBalloons(ref Grid gameField, ref List<BalloonVM> balloons, ref PlayerVM player)
        {
            bool IsNeedToCreate = (randomizer.NextDouble() > 0.5);
            if (IsNeedToCreate && balloons.Count <= GameSettings.MaximumBalloonsInGame)
            {
                Color newBallonColor;
                int newBallonColumn;
                RandomizeBalloon(out newBallonColor, out newBallonColumn);
                CreateBalloon(ref gameField, ref balloons, newBallonColumn, newBallonColor);
            }

            foreach (BalloonVM item in balloons)
            {
                bool IsNeedToMove = randomizer.NextDouble() > 0.7;
                if (IsNeedToMove)
                {
                    if (item.Row < GameSettings.GameGridRowSize - 1)
                    {
                        if (balloons
                            .Contains(balloons.FirstOrDefault(x => x.Row == item.Row + 1 && x.Column == item.Column)))
                        {
                            balloons
                                .FirstOrDefault(x => x.Row == item.Row + 1 && x.Column == item.Column).Row++;
                        }
                        else
                        {
                            item.Row++;
                        }
                    }
                    else
                    {
                        if (item.Column == player.PlayerColumnPosition)
                        {
                            player.IsPlayerCapturedBalloon = true;
                            player.CapturedColor = item.Color;
                            gameField.Children.Remove(gameField.Children.FirstOrDefault(x => x.BindingContext == item));
                            balloons.Remove(item);
                        }
                        else
                        {
                            gameField.Children.Remove(gameField.Children.FirstOrDefault(x => x.BindingContext == item));
                            balloons.Remove(item);
                        }
                    }
                    break;
                }
            }
        }

        public static void HandleSequence(ref PlayerVM player, ref GameStateVM gameState, ref Grid sequenceView, ref List<BalloonVM> sequence)
        {
            if (player.IsPlayerCapturedBalloon && player.CapturedColor == sequence.FirstOrDefault().Color)
            {
                gameState.Score += 100;
                player.IsPlayerCapturedBalloon = false;
                sequenceView.Children.RemoveAt(0);
                sequence.RemoveAt(0);
                foreach (BalloonVM item in sequence)
                {
                    if (item.Column > 0)
                    {
                        item.Column -= 1;
                    }
                }
                sequenceView.Children.ElementAt(0).Margin = GameSettings.FirstBalloonInSequenceMargin;
                Color newBalloonColor;
                RandomizeBalloonColor(out newBalloonColor);
                CreateBalloon(ref sequenceView, ref sequence, sequence.Count, newBalloonColor);
                isGameValid = true;
            }
            else
            {
                if (player.IsPlayerCapturedBalloon && player.CapturedColor != sequence.FirstOrDefault().Color)
                {
                    isGameValid = false;
                }
                else
                {
                    isGameValid = true;
                }
            }
        }

        public static void HandleTime(ref GameStateVM gameState)
        {
            if (gameState.Seconds < 60)
            {
                gameState.Seconds += 1;
            }
            else
            {
                gameState.Minutes += 1;
                gameState.Seconds = 0;
            }
        }

        public static async void HandleFail(GamePage page)
        {
            bool choice = await page.DisplayAlert("You failed!", "Do you want retry?", "Retry", "Exit");
            if (choice)
            {
                try
                {
                    Gyroscope.Stop();
                    Accelerometer.Stop();
                }
                finally
                {
                    GameHandler.InitHandler();
                    await page.Navigation.PushAsync(new GamePage());
                    page.Navigation.RemovePage(page);
                }
            }
            else
            {
                await page.Navigation.PushAsync(new MainPage());
                try
                {
                    Gyroscope.Stop();
                    Accelerometer.Stop();
                }
                finally
                {
                    page.Navigation.RemovePage(page);
                }
            }
        }

        public static void InitializeSequence(ref Grid sequenceView, ref List<BalloonVM> sequence)
        {
            for (int i = 0; i < GameSettings.SequenceCollectionSize; i++)
            {
                Color newBallonColor;
                RandomizeBalloonColor(out newBallonColor);
                CreateBalloon(ref sequenceView, ref sequence, i, newBallonColor);
            }
            sequenceView.Children.ElementAt(0).Margin = GameSettings.FirstBalloonInSequenceMargin;
        }

        public static void InitGameGrid(ref Grid gameField, ref List<BalloonVM> balloons)
        {
            Color newBallonColor;
            int newBallonColumn;
            RandomizeBalloon(out newBallonColor, out newBallonColumn);
            CreateBalloon(ref gameField, ref balloons, newBallonColumn, newBallonColor);
        }

        static void CreateBalloon(ref Grid gameField, ref List<BalloonVM> balloons, int Column, Color Color)
        {
            var balloon = new BalloonVM { Color = Color, Column = Column, Row = 0 };
            if (!balloons
                .Contains(balloons
                .FirstOrDefault(x => x.Column == balloon.Column && x.Row == balloon.Row)))
            {
                balloons.Add(balloon);
                var view = new SKCanvasView
                {
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    Margin = 0
                };
                gameField.Children.Add(view);
                view.BindingContext = balloon;
                view.SetBinding(VisualElement.BackgroundColorProperty, "Color");
                view.SetBinding(Grid.RowProperty, "Row");
                view.SetBinding(Grid.ColumnProperty, "Column");
                view.PaintSurface += OnPainting;
            }
        }

        static void RandomizeBalloon(out Color Color, out int Column)
        {
            double newBallonColorChance = randomizer.NextDouble();
            double newBallonColumnChance = randomizer.NextDouble();
            Color = newBallonColorChance < 0.33 ? Color.Red :
                (newBallonColorChance >= 0.33 && newBallonColorChance < 0.66) ? Color.Blue : Color.Yellow;
            Column = (int)Math.Floor(newBallonColumnChance / ((double)1 / GameSettings.GameGridColumnSize));
        }

        static void RandomizeBalloonColor(out Color Color)
        {
            double newBallonColorChance = randomizer.NextDouble();
            Color = newBallonColorChance < 0.33 ? Color.Red :
                (newBallonColorChance >= 0.33 && newBallonColorChance < 0.66) ? Color.Blue : Color.Yellow;
        }
        static void OnPainting(object sender, SKPaintSurfaceEventArgs e)
        {
            SKCanvasView view = sender as SKCanvasView;
            //получаем текущую поверхность из аргументов
            var surface = e.Surface;
            // Получаем холст на котором будет рисовать
            var canvas = surface.Canvas;
            // Очищаем холст
            canvas.Clear(SKColors.Black);

            var circleFill = new SKPaint
            {
                IsAntialias = true,
                Style = SKPaintStyle.Fill,
                Color = view.BackgroundColor.ToSKColor()
            };
            // Рисуем заполненный круг 
            canvas.DrawCircle(view.CanvasSize.Width / 2, view.CanvasSize.Height / 2, view.CanvasSize.Width / 2, circleFill);
        }
    }
}