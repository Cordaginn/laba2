﻿using System.ComponentModel;
using Xamarin.Forms;

namespace BaloonsGame.ViewModels
{
    public class PlayerVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private Player Player;

        public PlayerVM()
        {
            Player = new Player();
        }

        public bool IsPlayerCapturedBalloon
        {
            get { return Player.IsCaptured; }
            set
            {
                if (Player.IsCaptured != value)
                {
                    Player.IsCaptured = value;
                    OnPropertyChanged("IsPlayerCapturedBallon");
                }
            }
        }

        public int PlayerRowPosition
        {
            get { return Player.Row; }
            set
            {
                if (Player.Row != value)
                {
                    Player.Row = value;
                    OnPropertyChanged("PlayerRowPosition");
                }
            }
        }

        public int PlayerColumnPosition
        {
            get { return Player.Column; }
            set
            {
                if (Player.Column != value)
                {
                    Player.Column = value;
                    OnPropertyChanged("PlayerColumnPosition");
                }
            }
        }

        public Color CapturedColor
        {
            get { return Player.CapturedColor; }
            set
            {
                if (Player.CapturedColor != value)
                {
                    Player.CapturedColor = value;
                    OnPropertyChanged("CapturedColor");
                }
            }
        }

        protected void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
    }
}
