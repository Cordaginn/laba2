﻿using System.ComponentModel;

namespace BaloonsGame.ViewModels
{
    public class GameStateVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private GameState State;

        public GameStateVM()
        {
            State = new GameState();
        }

        public int Score
        {
            get { return State.Score; }
            set
            {
                if (State.Score != value)
                {
                    State.Score = value;
                    OnPropertyChanged("Score");
                }
            }
        }

        public int Minutes
        {
            get { return State.Minutes; }
            set
            {
                if (State.Minutes != value)
                {
                    State.Minutes = value;
                    OnPropertyChanged("Minutes");
                }
            }
        }
        public int Seconds
        {
            get { return State.Seconds; }
            set
            {
                if (State.Seconds != value)
                {
                    State.Seconds = value;
                    OnPropertyChanged("Seconds");
                }
            }
        }

        protected void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
    }
}
