﻿using System.ComponentModel;
using Xamarin.Forms;

namespace BaloonsGame.ViewModels
{
    public class BalloonVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public Balloon balloon { get; private set; }

        public BalloonVM()
        {
            balloon = new Balloon();
        }

        public Color Color
        {
            get { return balloon.Color; }
            set
            {
                if (balloon.Color != value)
                {
                    balloon.Color = value;
                    OnPropertyChanged("Color");
                }
            }
        }

        public int Column
        {
            get { return balloon.Column; }
            set
            {
                if (balloon.Column != value)
                {
                    balloon.Column = value;
                    OnPropertyChanged("Column");
                }
            }
        }

        public int Row
        {
            get { return balloon.Row; }
            set
            {
                if (balloon.Row != value)
                {
                    balloon.Row = value;
                    OnPropertyChanged("Row");
                }
            }
        }

        protected void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
    }
}
