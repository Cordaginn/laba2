﻿using BaloonsGame.Models;
using System.Collections.Generic;
using System.ComponentModel;
using System.Numerics;

namespace BaloonsGame.ViewModels
{
    public class SensorVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private Sensor sensor;

        public SensorVM()
        {
            sensor = new Sensor();
            sensor.gyroDataFilter = new Stack<Vector3>();
            sensor.accelDataFilter = new Stack<Vector3>();
        }

        public Vector3 DataGyro
        {
            get { return sensor.dataGyro; }
            set
            {
                if (sensor.dataGyro != value)
                {
                    sensor.dataGyro = value;
                    OnPropertyChanged("dataGyro", sensor.dataGyro);
                }
            }
        }

        public Vector3 DataAccel
        {
            get { return sensor.dataAccel; }
            set
            {
                if (sensor.dataAccel != value)
                {
                    sensor.dataAccel = value;
                    OnPropertyChanged("dataAccel", sensor.dataAccel);
                }
            }
        }

        public float FilteredDataAccel
        {
            get { return sensor.filteredAccelData; }
            set
            {
                if (sensor.filteredAccelData != value)
                {
                    sensor.filteredAccelData = value;
                    PropertyChanged(this, new PropertyChangedEventArgs("filteredAccelData"));
                }
            }
        }

        public float FilteredDataGyro
        {
            get { return sensor.filteredGyroData; }
            set
            {
                if (sensor.filteredGyroData != value)
                {
                    sensor.filteredGyroData = value;
                    PropertyChanged(this, new PropertyChangedEventArgs("filteredGyroData"));
                }
            }
        }

        public void filterData()
        {
            float theta = (float)0.4;
            Vector3 xk;
            Vector3 yk1;
            float X_xk;
            float Z_xk;
            float X_yk1;
            float Z_yk1;

            if (sensor.gyroDataFilter.Count >= 2)
            {
                xk = sensor.gyroDataFilter.Pop();
                yk1 = sensor.gyroDataFilter.Pop();
                Z_xk = xk.Z;
                Z_yk1 = yk1.Z;
                FilteredDataGyro = theta * Z_xk + (1 - theta) * Z_yk1;
            }
            if (sensor.accelDataFilter.Count >= 2)
            {
                xk = sensor.accelDataFilter.Pop();
                yk1 = sensor.accelDataFilter.Pop();
                X_xk = xk.X;
                X_yk1 = yk1.X;
                FilteredDataAccel = theta * X_xk + (1 - theta) * X_yk1;
            }
        }

        public void OnPropertyChanged(string propName, Vector3 data)
        {
            if (PropertyChanged != null && propName == "dataAccel")
            {
                if (sensor.accelDataFilter.Count <= 1)
                {
                    sensor.accelDataFilter.Push(data);
                }
                else
                {
                    filterData();
                }
            }
            else
            {
                if (PropertyChanged != null && propName == "dataGyro")
                {
                    if (sensor.gyroDataFilter.Count <= 1)
                    {
                        sensor.gyroDataFilter.Push(data);
                    }
                    else
                    {
                        filterData();
                    }
                }
            }
        }
    }
}
