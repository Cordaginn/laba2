﻿using Xamarin.Forms;

namespace BaloonsGame
{
    public class Balloon
    {
        public Color Color { get; set; }
        public int Column { get; set; }
        public int Row { get; set; }
    }
}
