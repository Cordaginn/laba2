﻿namespace BaloonsGame
{
    public class GameState
    {
        public int Score { get; set; }
        public int Seconds { get; set; }
        public int Minutes { get; set; }
    }
}
