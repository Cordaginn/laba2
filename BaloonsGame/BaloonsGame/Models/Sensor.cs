﻿using System.Collections.Generic;
using System.Numerics;

namespace BaloonsGame.Models
{
    public class Sensor
    {
        public Vector3 dataGyro { get; set; }
        public Vector3 dataAccel { get; set; }
        public float filteredAccelData { get; set; }
        public float filteredGyroData { get; set; }
        public Stack<Vector3> gyroDataFilter;
        public Stack<Vector3> accelDataFilter;
    }
}
