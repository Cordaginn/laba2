﻿using Xamarin.Forms;

namespace BaloonsGame
{
    public class Player
    {
        public int Column { get; set; }
        public int Row { get; set; }
        public bool IsCaptured { get; set; }
        public Color CapturedColor { get; set; }
    }
}
