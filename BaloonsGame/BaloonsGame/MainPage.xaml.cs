﻿using BaloonsGame.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace BaloonsGame
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(true)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private async void StartGame(object sender, EventArgs e)
        {
            GameHandler.InitHandler();
            await Navigation.PushAsync(new GamePage());
            Navigation.RemovePage(this);
        }

    }
}
