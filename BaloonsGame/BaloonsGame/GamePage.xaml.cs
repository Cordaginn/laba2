﻿using BaloonsGame.Core;
using BaloonsGame.Views;
using System;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BaloonsGame
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GamePage : ContentPage
    {
        PlayerView playerView;
        GameGridView gameView;
        GameStateView stateView;
        BottomGUI bottomView;
        public GamePage()
        {
            InitializeComponent();
            gameView = new GameGridView();
            playerView = new PlayerView();
            stateView = new GameStateView();
            bottomView = new BottomGUI();
            bottomView.exitButton.Clicked += ExitGame;

            Content = new StackLayout
            {
                BackgroundColor = Color.Black,
                Children =
                {
                    stateView,
                    gameView,
                    playerView,
                    bottomView
                }
            };
            gameView.GestureRecognizers.Add(playerView.gestureRecognizerLeft);
            gameView.GestureRecognizers.Add(playerView.gestureRecognizerRight);
            GameHandler.InitGameGrid(ref gameView.gameField, ref gameView.balloonsCollection);
            Device.StartTimer(TimeSpan.FromSeconds(1), GameTick);
        }

        private bool GameTick()
        {
            if (GameHandler.isGameValid && !GameHandler.isGamePaused)
            {
                for (int i = 0; i < stateView.gameState.Minutes + 1; i++)
                {
                    GameHandler.HandleBalloons(ref gameView.gameField, ref gameView.balloonsCollection, ref playerView.player);
                    GameHandler.HandleSequence(ref playerView.player, ref stateView.gameState, ref bottomView.sequenceView, ref bottomView.sequence);
                }
                GameHandler.HandleTime(ref stateView.gameState);
                return true;
            }
            else
            {
                if (!GameHandler.isGameValid)
                {
                    GameHandler.HandleFail(this);
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        private async void ExitGame(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MainPage());
            try
            {
                Gyroscope.Stop();
                Accelerometer.Stop();
            }
            finally
            {
                Navigation.RemovePage(this);
            }
        }
    }
}