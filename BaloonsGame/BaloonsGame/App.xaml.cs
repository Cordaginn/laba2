﻿using System;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BaloonsGame
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage()) {BarBackgroundColor=Color.Black };
        }

        protected override void OnStart()
        {

            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            Accelerometer.Stop();
            Gyroscope.Stop();
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            Accelerometer.Start(GameSettings.SensorSpeed);
            Gyroscope.Start(GameSettings.SensorSpeed);
            // Handle when your app resumes
        }
    }
}
